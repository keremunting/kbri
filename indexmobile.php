<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('./tools/conf.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Template that use Bootstrap</title>

    <!-- Referencing Bootstrap CSS that is hosted locally -->

    <script src="./bootstrap/js/jquery-1.12.0.min.js"></script>
<script src="./bootstrap/js/bootstrap.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>
<script src="./bootstrap/js/bootstrap-datetimepicker.min.js"></script>
<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap-theme.css">
<link href="./css/formValidation.css" rel="stylesheet">
<link href="./css/kbri.css" rel="stylesheet">

<link href="./css/custom.css" rel="stylesheet">


<link rel="stylesheet" href="themes/base/jquery.ui.all.css">
<script src="./js/jquery-ui.js"></script>
<script src="./js/validasi.js"></script>

<script src="./js/bootstrapValidator.min.js"></script>


</head>
<body>

<div class="header" style ="background-color: #663300; text-align:center">
        <a class="judul"><h3><img class="img-responsive2" src="./img/garuda-perwakilan.png" style="padding:10px 10px 10px 10px;"></a>
        <br><font color='#fef'>Embassy of the Republic of Indonesia to the Kingdom of Thailand</font></h3>
</div>
<div class="content">
      <form role="form" id="form" method="POST" action="load.php" enctype="multipart/form-data">
      <div class="col-sm-13">
        <div class="col-sm-13">
        <div class="headerform">IDENTITAS DIRI</div>  
                <div class="form-group">
                  <label>Nama Lengkap : <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  <input type="text" name="nama" size="10" placeholder="Nama Lengkap" class="form-control" maxlength="100">
                  
                </div>
                <!--- untuk foto ---->
                  <label>Upload Foto Halaman Depan Paspor <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  <div class="col-sm-13">
                   <input type="file" accept="image/*" onchange="loadFile(event)" name="filegbr" id="filegbr">

                          <img id="output" height="150" width="130" /><br/><br/>
                            <div id="coba"><strong>Type File PNG atau JPG</strong></div>
                            <script>
                            var loadFile = function(event) {
                          
                              document.getElementById("output").innerHTML = "<img src='file://"+filegbr+"' border='0'>";
                                var output = document.getElementById('output');
                                var file = event.target.files[0].name; //Ambil Value
                                var ekstensi = ['jpg','png']; //Variabel array untuk penentuan Ekstensi

                                if ( file ) {
                                  var ambilekstensi = file.split('.');  //Ambil Ekstensi
                                      ambilekstensi = ambilekstensi.reverse();
                                  if ( $.inArray ( ambilekstensi[0].toLowerCase(), ekstensi ) != -1 ){
                                        output.src = URL.createObjectURL(event.target.files[0]);  //jika cocok return true
                                  }
                                  else {
                                      $("#filegbr").replaceWith($("#filegbr").clone(true));
                                        //For other browsers
                                      $("#filegbr").val("");
                                      document.getElementById("output").remove();
                                      document.getElementById("coba").innerHTML = "<Strong><font style=\"color:#0000ff;\">WARNING !! <br/>Type file harus jpg atau png</font></strong>";
                                    }
                                  }
                                else{

                                }
                              };
                            </script>
                            <!--<script>
                              var loadFile = function(event) {
                                var output = document.getElementById('output');

                                output.src = URL.createObjectURL(event.target.files[0]);
                              };
                            </script>
                            -->
                        
                      
                    
                  </div>
                
              
              
                
                  <div class="form-group">
                    <label>Tempat Lahir : <sup style="color:red;">(* wajib diisi)</p></sup></label>
                    <input type="text" name="tempat_lahir" size="50" placeholder="Tempat Lahir" class="form-control" maxlength="200">
                  </div>
                
              
              
                	
                    <div class="form-group">
                    	    <label>Tanggal Lahir : <sup style="color:red;">(* wajib diisi)</p></sup></label>
                            <div class="input-group add-on">
                                <input type="text"name="tgl_lahir_user" id="datepicker7"  class="form-control" placeholder="DD/MM/YYYY" maxlength="10">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                
              
              
                
                <div class="form-group">
                  <label>Agama : <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  <select name="agama" class="form-control" id="agama">
                    <option value=""> .:: Pilih Agama ::. </option>
                    <option value="Islam">Islam </option>
                    <option value="kristen Protestan">Kristen Protestan</option>
                    <option value="kristen Katolik">Kristen Katolik</option>
                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                    <option value="Budha">Budha</option>
                    <option value="Hindu">Hindu</option>
                  </select>
                </div>
                
              
              

                
                <div class="form-group">
                  <label>Status : <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  
                  <labe><input type="radio" name="status" value="Belum menikah" class="form-control">Belum Menikah</label>
                  <labe><input type="radio" name="status" value="menikah" class="form-control">Menikah</label>
                  <labe><input type="radio" name="status" value="cerai" class="form-control">Cerai</label>
                  </labe>
                
              
              <div class="col-sm-13">  
                  <div class="form-group">
                  <label>Jenis Kelamin : <sup style="color:red;">(* wajib diisi)</p></sup></label><br>
                  <label><input type="radio" name="jk" value="L">Laki-Laki</label>
                  <label><input type="radio" name="jk" value="P">Perempuan</label>
                  </div>
                
              
              
              	  <div class="form-group">
              	  <label>Email : <sup style="color:red;">(* wajib diisi)</p></label>
                  <input type="email" name="email" maxlength="200" class="form-control" placeholder="xxxx@xxx.xxx">
              	  </div>
              	  
                  <label>Nama Suami / Istri : </label>
                  <input type="text" name="nama_suami_istri" placeholder="Nama Suami / Istri" class="form-control" maxlength="200">
                
              
              </div>
              <div class="col-sm-13">
              
                
                  <label>Jumlah Anak : </label>
                    <select class="form-control" id="jmlanak" name="jmlanak">
                      <option value='' selected="">.:: Jumlah anak ::.</option>
                      <option value='-1'> Belum ada </option>
                      <?php
                        for($i=1;$i<=30;$i++){
                            echo"<option value='$i'>$i</option>";
                        }
                      ?>
                    </select>  &nbsp; (* Di isi Jika Mempunyai anak

                  <div id="holder"></div>
                
              
     </tbody>
  
  <br><div class="headerform">Data diri di Indonesia</div>

  
          

            
            <div class="form-group">
                <label>Alamat Rumah : <sup style="color:red;">(* wajib diisi)</p></label>
                <input type="text" name="alamatrumahi"  class="form-control" placeholder="Alamat rumah nama jalan dan Nomor" maxlength="200">
            </div>
            
          
          
            
            <div class="form-group">
                <label>Kota : <sup style="color:red;">(* wajib diisi)</p></label>
                <input type="text" name="kotai"  class="form-control" placeholder="Kota" maxlength="100">
            </div>
            
            
            <div class="form-group">
                <label>Provinsi : <sup style="color:red;">(* wajib diisi)</p></label>
                <input type="text" name="provinsii"  class="form-control" placeholder="Provinsi" maxlength="100">
            </div>
            
          
          
            
            <div class="form-group">
                <label>Kode Pos : <sup style="color:red;">(* wajib diisi)</p></label>
                <input type="text" name="Kode_posi"  class="form-control" maxlength="7" placeholder="Kode pos">
            </div>
            
            
            <div class="form-group">
                <label>No. Telp : <sup style="color:red;">(* wajib diisi)</p></label>
                <input type="text" name="no_tlpi"  class="form-control" placeholder="No tlpn" maxlength="15">
            </div>
            
          
          
  </tbody>

  </div>
  <div class="col-sm-13">
  <div class="headerform">Informasi Paspor</div>
  
              
              
                
                  <label>Jenis Paspor <sup style="color:red;">(* wajib diisi)</p></sup></label>
                
                    <div class="form-group">
                    <label>Nomor Paspor & Tempat dikeluarkan <sup style="color:red;">(* wajib diisi)</p></sup></label><br/>
                    <label><input type="radio" name="paspor" value="biasa">Biasa</label><BR>
                    <label><input type="radio" name="paspor" value="dinas">Dinas</label><BR>
                    <label><input type="radio" name="paspor" value="diplomatik">Diplomatik</label>
                   </div>
                  
                    <div class="form-group">
                    <label>Nomor Paspor <sup style="color:red;">(* wajib diisi)</p></sup></label>
                    <input type="text" name="nomor_paspor" maxlength="100" placeholder="Nomor Paspor" class="form-control"> 
     		    </div>
     		    
     		    <div class="form-group">
 		    <label>Tempat Keluar <sup style="color:red;">(* wajib diisi)</p></sup></label>
                    <input type="text" name="tempat_keluar" maxlength="200" placeholder="Tempat dikeluarkan" class="form-control">
    		    </div>
                
                    <label>Tanggal dikeluarkan <sup style="color:red;">(* wajib diisi)</p></sup></label>
                    <label>Berlaku Sampai dengan <sup style="color:red;">(* wajib diisi)</p></sup></label>
                        <div class="form-group">
                        <div class="input-group add-on">
                        
                            <input type="text" name="tanggalkp" id="datepicker1" class="form-control" placeholder=" Tanggal dikeluarkan DD/MM/YYYY" maxlength="10" >
                            
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span><br/>
                            </div>
                        </div>
                  
                  <div class="form-group">
                            <div class="input-group add-on">
                           
                                <input type="text" name="tanggalap" id="datepicker2"  class="form-control" placeholder="Tanggal akhir berlaku DD/MM/YYYY" maxlength="10">
                            
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                               </div>
                            </div>
                  
                 
                
      </tbody>
    
    <div class="headerform">Data Diri di Thailand</div>
    
      
              
                
                    <label>Alamat rumah <sup style="color:red;">(* wajib diisi)</p></sup></label>
                    <div class="form-group">
                    <input type="text" maxlength="100" name="alamat_rumaht"  class="form-control" placeholder="Alamat rumah nama jalan dan Nomor">
                    </div>
              
              
                
                    <label>Kode Pos: <sup style="color:red;">(* wajib diisi)</p></sup> </label>
                    <div class="form-group">
                    <input type="text" maxlength="7" name="Kode_post"  class="form-control" placeholder="Kode pos">
                    </div>
                
                    <label> Kota : <sup style="color:red;">(* wajib diisi)</p></sup> </label>
                    <div class="form-group">
                    <input type="text" name="kotat" maxlength="100"  class="form-control" placeholder="Kota">
                    </div>
              
              
                
                   <label>No Telp : <sup style="color:red;">(* wajib diisi)</p></sup> </label>
                   <div class="form-group">
                  <input type="text" name="tlpt" maxlength="15" class="form-control" placeholder="16 Digit Angka">
                  </div>
                
                  <label>Pekerjaan : <sup style="color:red;">(* wajib diisi)</p></sup> </label>
                   <div class="form-group">
                  <input type="text" name="pekerjaant" maxlength="200" class="form-control" placeholder="Pekerjaan">
                   </div>
              
              
                
                  <label>Nama Tempat kerja/Sekolah :  <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  <div class="form-group">
                  <input type="text" name="tmptkerjasekolah" maxlength="200" class="form-control" placeholder="Nama tempat kerja">
                  </div>
                
                  <label>No Tlpn tempat kerja/Sekolah : <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  <div class="form-group">
                  <input type="text" name="tlptmptkerjasekolah" maxlength="15" class="form-control" placeholder="16 Digit Angka">
                  </div>
              
              
                
                  <label>Alamat Kantor / sekolah : <sup style="color:red;">(* wajib diisi)</p></sup> </label>
                  <div class="form-group">
                    <input type="text" name="alamatkantorsekolah" maxlength="200"  class="form-control" placeholder="Alamat kantor/sekolah nama jalan dan Nomor">
                 </div>
              
              
                
                  <label>Kode Pos Kantor Sekolah : <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  <div class="form-group">
                    <input type="text" name="Kodeposkantorsekolah" maxlength="7"  class="form-control" placeholder="Kode pos kantor/sekolah">
                  </div>
                
                  <label>Kota Kantor Sekolah :  <sup style="color:red;">(* wajib diisi)</p></sup></label>
                  <div class="form-group">
                    <input type="text" name="kotakantorsekolah" maxlength="100"  class="form-control" placeholder="Kota kantor/sekolah">
                 </div>
              
        </tbody>
    
    </div>
    </div>
    <div class="col-sm-13">
    <div class="headerform">KONTAK YANG DAPAT DIHUBUNGI APABILA TERDAPAT SITUASI DARURAT</div>
        <div class="headerform">Di Thailand</div>
        
          
            
              <div class="form-group">
              <label>Nama Lengkap :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="rnamalengkapht"  maxlength="200" class="form-control" placeholder="Nama Lengkap">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Hubungan dengan pendaftar :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="statushubungant" maxlength="200" class="form-control" placeholder="Hubungan dengan pendaftar">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Alamat (*Nama Jalan dan Nomor) :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="alamathubunganht" maxlength="200" class="form-control" placeholder="alamat">
              </div>
            
          
          
            
              <label>Kode pos :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="Kodeposht" maxlength="7" class="form-control" placeholder="Kode pos">
            
          
          
            
              <div class="form-group">
              <label>Kota :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="kotaht" maxlength="200" class="form-control" placeholder="Kota">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Nomor tlpn :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="notlpht" maxlength="15" class="form-control" placeholder="xxxxxxx">
              </div>
            
          
          
            
              <div class="form-group">
              <label>No Seluler :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="nosht" maxlength="15" class="form-control" placeholder="xxxxxxxx">
              </div>
            
  
            
              <div class="form-group">
              <label>Alamat Email :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="email" name="emailht" maxlength="200" class="form-control" placeholder="Email@email.com">
              </div>
            
          
        <div class="headerform">Di Indonesia</div>
        
          
            
              <div class="form-group">
              <label>Nama Lengkap :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="rnamalengkaphi" maxlength="200" class="form-control" placeholder="Nama Lengkap">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Hubungan dengan pendaftar :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="statushubungani" maxlength="200" class="form-control" placeholder="Hubungan dengan pendaftar">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Alamat (*Nama Jalan dan Nomor) :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="alamathubunganhi" maxlength="200" class="form-control" placeholder="alamat">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Kode pos :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="Kodeposhi" maxlength="7" class="form-control" placeholder="Kode pos">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Kota :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="kotahi" maxlength="200" class="form-control" placeholder="Kota">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Nomor tlpn :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="notlphi" maxlength="15" class="form-control" placeholder="xxxxxxx">
              </div>
            
          
          
            
              <div class="form-group">
              <label>No Seluler :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="text" name="noshi" maxlength="15" class="form-control" placeholder="xxxxxxxx">
              </div>
            
          
          
            
              <div class="form-group">
              <label>Alamat Email :<sup style="color:red;">(* wajib diisi)</p></label>
              <input type="email" name="emailhi" maxlength="200" class="form-control" placeholder="Email@email.com">
              </div>
            
    </div>
    <div class="col-sm-13">
      <div class="headerform">ISIAN BAGI PENDAFTARAN ANAK DI BAWAH USIA 18 TAHUN OLEH ORANG TUA</div>
          <div class="headerform">Data Ayah</div>
            
              
                
                  <label>Nama Lengkap</label>
                  <input type="text" name="nama_lengkap_ayah" class="form-control" placeholder="Nama Lengkap">
                
              
              
                
                  <label>Tempat dan Tanggal Lahir</label>
                
              
              
                
                  <input type="text" name="tempat_lahir_ayah" class="form-control" placeholder="Tempat Lahir">
                
                
                <div class="form-group">
                            <div class="input-group add-on">
                                <input type="text"name="tgl_lahir_ayah" id="datepicker3"  class="form-control" placeholder="DD/MM/YYYY" maxlength="10">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                  
                
              
              
                
                  <label>Kewarganegaraan</label>
                  <input type="text" name="kewarganegaraan_ayah" class="form-control" placeholder="Kewarganegaraan">
                  <label>No. Paspor</label>
                  <input type="text" name="no_paspor_ayah" class="form-control" placeholder="Nomor Paspor">
          <br>
          <div class="headerform">Data Ibu</div>
          
            
              
                <label>Nama Lengkap</label>
                <input type="text" name="nama_lengkap_ibu" class="form-control" placeholder="Nama Lengkap">
              
            
            
              
                <label>Tempat dan Tanggal Lahir</label>
              
      
                <input type="text" name="tempat_lahir_ibu" class="form-control" placeholder="Tempat Lahir">
              
              <br>
                  <div class="form-group">
                            <div class="input-group add-on">
                                <input type="text" name="tgl_ibu" id="datepicker4"  class="form-control" placeholder="DD/MM/YYYY" maxlength="10">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
              
            
            
              
                <label>Kewarganegaraan</label>
                <input type="text" name="kewarganegaraan_ibu" class="form-control" placeholder="Kewarganegaraan">
              
            
            
              
                <label>No. Paspor</label>
                <input type="text" name="no_paspor_ibu" class="form-control" placeholder="Nomor Paspor">
              
            
          
        </div>
        <div class="col-sm-13">
          <div class="headerform">Data yang bersangkutan</div>
            <div class="col-sm-13">
              
                
                  
                    <label>Apakah yang bersangkutan (yang didaftarkan) memiliki kewarganegraan yang terbatas ? </label>
                    <br><label><input type="radio" name="memiliki_kewarganegraan_terbatas" value="Y">Ya</label>
                    <label><input type="radio" name="memiliki_kewarganegraan_terbatas" value="T">Tidak</label>
                  
                
                
                  
                    <br><label>Apabila memiliki kewarganegaraan ganda terbatas, sebutkan kewarganegaraan lainnya:</label>
                    <input type="text" name="nama_kewarganegaraan_lain" placeholder="Nama Kewarganegaraan lain" class="form-control">
                  
                
              
            </div>
        </div>
    </div>
    <br>
    <div class="col-sm-13">
      <div class="headerform"> INFORMASI KEKONSULERAN </div>
      <div class="col-sm-13">
    
                <div class="form-group">
                <label>Tanggal ketibaan di Thailand <i>(tanggal/bulan/tahun)</i></label>
                <div class="form-group">
                            <div class="input-group add-on">
                                <input type="text" name="tanggal_tiba" id="datepicker5"  class="form-control" placeholder="DD/MM/YYYY" maxlength="10">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
               
                </div><br/>
               <div class="form-group">
                <label>Lama Tinggal sampai dengan tanggal <i>(tanggal/bulan/tahun)</i></label>
                <div class="form-group">
                            <div class="input-group add-on">
                                <input type="text" name="lamatinggal" id="datepicker8"  class="form-control" placeholder="DD/MM/YYYY" maxlength="10">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
               
                </div>
            
          <br/>
        </div>
                <div class="form-group">
                <label>Maksud tinggal di Thailand</label><br>
                <div class="checkbox">
                  <label><input type="checkbox" name="mtinggalt[]" value="bekerja"> Bekerja</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="mtinggalt[]" value="sekolah"> Sekolah</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="mtinggalt[]" value="menetap"> Menetap</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="mtinggalt[]" value="liburan"> Liburan </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="mtinggalt[]" value="lainnya"> Lainnya
                    <input type="text" name="lainalasan" class="form-control" placeholder="Sebutkan">
                  </label>
                </div>
                </div>
              
            
          
      </div>      
    <div class="col-sm-13">
        <div class="headerform">Tempat dan Tanggal pengisian Fomulir</div>
          
            
              
                <div class="form-group">
                <label>Tempat</label>
                <input type="text" name="tempattpf" placeholder="Tempat Pengisian Formulir" class="form-control">
                </div>
              
            
          
        </div>
          
            
              
                <div class="form-group">
                <label>Tanggal</label>
                <div class="form-group">
                            <div class="input-group add-on">
                                <input type="text" name="tanggalttpf" id="datepicker6"  class="form-control" placeholder="DD/MM/YYYY" maxlength="10">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                
                </div>
              
            
      </div>
      <div class="col-sm-13" align="center">
        <img src="captcha.php"><br><br>
        <label><input type="text" name="captcha" class="form-control" placeholder="Captcha"></label>
        <div class="form-group">
        <div class="checkbox">
          <input type="checkbox" name="pernyataan" value="1"> Saya mengisi formulir dengan benar dan penuh kesadaran.
        </div>
        </div>

        <div class="form-group">
        <div class="col-xs-9 col-xs-offset-3">
            <button type="submit" class="btn btn-primary btn-lg" value="Daftakan">Send</button>
        </div>
    </div>
      </div>
      </div>
    </div>
  </form>
</div>
</div>
<footer class="container-fluid">
  <p>&copy; 2016.</p>
</footer>
    <!--<div class="footer">
    </div>-->

</body>
</html>
