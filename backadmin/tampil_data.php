<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
/*error_reporting(E_ALL); */
error_reporting(0);
function tgl_convert($tgl){
			$exploded = explode('-',$tgl);
			$imploded = $exploded[2].'-'.$exploded[1].'-'.$exploded[0];
			return $imploded;
}
$id = mysqli_real_escape_string($con, $_GET['ids']);
$sql1 = "SELECT
        tidentitas.id_identitas as idIdentitas,
        tidentitas.nama as nama,
        tidentitas.foto as foto,
        tidentitas.tempat_lahir as tempatLahir,
        tidentitas.tgl_lahir_user as tglLahirUser,
        tidentitas.agama as agama,
        tidentitas.status as status,
        tidentitas.jk as jenisKelamin,
        tidentitas.email as emailUser,
        tidentitas.nama_suami_istri as namaSuamiIstri,
        tidentitas.jmlanak as jumlahAnak,
        tidentitas.alamatrumahi as alamatRumahIndo,
        tidentitas.kotai as kotaIndo,
        tidentitas.provinsii as provinsiIndo,
        tidentitas.Kode_posi as kodePosIndo,
        tidentitas.no_tlpi as noTelpIndo,
        tidentitas.paspor as paspor,
        tidentitas.nomor_paspor as nomorPaspor,
        tidentitas.tempat_keluar as tempatKeluar,
        tidentitas.tanggalkp as tanggalKp,
        tidentitas.tanggalap as tanggalAp,
        tidentitas.alamat_rumaht as alamatRumahThai,
        tidentitas.Kode_post as kodePosThai,
        tidentitas.kotat as kotaThai,
        tidentitas.tlpt as telpThai,
        tidentitas.pekerjaant as pekerjaanThai,
        tidentitas.tanggal_tiba as tanggalTiba,
        tidentitas.maksudtinggal as maksudTinggal,
        tidentitas.nama_kewarganegaraan_lain as namaKewarganegaraanLain,
        tidentitas.tempattpf as tempattpf,
        tidentitas.tanggalttpf as tanggalttpf,
        tidentitas.lamatinggal as lamatinggal,

        tempat_kerja_kuliah.tmptkerjasekolah as tempatKerjaSekolah,
        tempat_kerja_kuliah.tlptmptkerjasekolah as telpTempatKerjaSekolah,
        tempat_kerja_kuliah.alamatkantorsekolah as alamatKantorSekolah,
        tempat_kerja_kuliah.Kodeposkantorsekolah as kodePosKantorSekolah,
        tempat_kerja_kuliah.kotakantorsekolah as kotakantorSekolah
        FROM tidentitas
        JOIN tempat_kerja_kuliah ON tempat_kerja_kuliah.id_identitas = tidentitas.id_identitas
        WHERE tidentitas.id_identitas = $id GROUP BY tidentitas.id_identitas";
  $sql2 = "SELECT nama_lengkap as namaLengkapOrtu, tempat_lahir as tempatLahirOrtu,
          tgl_lahir as tanggalLahirOrtu, kewarganegaraan as kewarganegaraanOrtu,
          no_paspor as noPasporOrtu, status as status
          FROM torang_tua
          WHERE id_identitas = $id";
  $sql3 = "SELECT nama_anak FROM tanak WHERE id_identitas = $id";
  $sql4 = "SELECT
          namalengkap as namaLengkapDarurat,
          statushubungan as statusHubunganDarurat,
          alamathubungan as alamatHubunganDarurat,
          Kodepos as kodePosDarurat,
          kota,
          notlp as noTelpDarurat, nos as nosDarurat, email as emailDarurat,
          negara as negaraDarurat
          FROM table_kontak_darurat
          WHERE id_identitas = $id";
  $res1 = mysqli_query($con,$sql1);
  if (mysqli_num_rows($res1) > 0)     
{  
  $res2 = mysqli_query($con,$sql2);
  $res3 = mysqli_query($con,$sql3);
  $res4 = mysqli_query($con,$sql4);

  $d = mysqli_fetch_assoc($res1);
    $idIdentitas  = $d['idIdentitas'];
    $nama = $d['nama'];
    $foto = $d['foto'];
    $tempatLahir = $d['tempatLahir'];
    $tglLahirUser = tgl_convert($d['tglLahirUser']);
    $agama = $d['agama'];
    $status = $d['status'];
    $jenisKelamin = $d['jenisKelamin'];
    $emailUser = $d['emailUser'];
    $namaSuamiIstri = $d['namaSuamiIstri'];
    $jumlahAnak = $d['jumlahAnak'];
    $alamatRumahIndo = $d['alamatRumahIndo'];
    $kotaIndo = $d['kotaIndo'];
    $provinsiIndo = $d['provinsiIndo'];
    $kodePosIndo = $d['kodePosIndo'];
    $noTelpIndo = $d['noTelpIndo'];
    $paspor = $d['paspor'];
    $nomorPaspor = $d['nomorPaspor'];
    $tempatKeluar = $d['tempatKeluar'];
    $tanggalKp = tgl_convert($d['tanggalKp']);
    $tanggalAp = tgl_convert($d['tanggalAp']);
    $alamatRumahThai = $d['alamatRumahThai'];
    $kodePosThai = $d['kodePosThai'];
    $kotaThai = $d['kotaThai'];
    $telpThai = $d['telpThai'];
    $pekerjaanThai = $d['pekerjaanThai'];
    $tanggalTiba = tgl_convert($d['tanggalTiba']);
    $maksudTinggal = $d['maksudTinggal'];
    $namaKewarganegaraanLain = $d['namaKewarganegaraanLain'];
    $tempattpf = $d['tempattpf'];
    $tanggalttpf = tgl_convert($d['tanggalttpf']);

    $tempatKerjaSekolah = $d['tempatKerjaSekolah'];
    $telpTempatKerjaSekolah = $d['telpTempatKerjaSekolah'];
    $alamatKantorSekolah = $d['alamatKantorSekolah'];
    $kodePosKantorSekolah = $d['kodePosKantorSekolah'];
    $kotakantorSekolah = $d['kotakantorSekolah'];
    $lamatinggal = tgl_convert($d['lamatinggal']);
?>
<style>
.tables { clear:both; padding: 20px; }
.tables tr { line-height: 30px; }
.tables td {padding: 8px; border-bottom: 1px solid #ddd; }
</style>
 <?php echo "<a href='download.php?ids=$id' role='button' class='btn btn-success btn-large'>Download</a>"; ?>
   <table cellpadding="10" cellspacing="10" class="tables">
        <tbody>
        <tr>
                <td colspan="3" bgcolor="#000000" align="center"><h3 style="color:#ffffff;">DATA IDENTITAS PELAPOR
        </tr>
         <tr>
                <td style="width: 20%;">
                    No registrasi
                </td>
                <td style="width: 10%;"> :  </td>
                <td style="width:70%;">
                  <?php echo $id;?>
                </td>
        </tr>
        <tr>
                <td style="width: 20%;">
                    Nama Lengkap
                </td>
                <td style="width: 10%;"> :  </td>
                <td style="width:70%;">
                  <?php echo $nama;?>
                </td>
        </tr>
        <tr>
              <td style="width: 20%;"> FOTO
                </td>
                <td> :  </td>
                <td style="width:70%;">
                            <?php echo "<img src='../images/$foto' width='150' height='180'>"; ?>
                </td>

        </tr>
        <tr>
                <td style="width: 20%;">
                    Tempat Lahir / Tanggal lahir
                  </td>
                  <td> : </td>
                  <td style='width:70%;'>
                    <?php echo $tempatLahir;?> / <?php echo $tglLahirUser;?>
                </td>
        </tr>
        <tr>
                <td style="width: 20%;">
                  Agama
                </td>
                <td> : </td>
                <td style='width:70%;'>
                  <?php echo $agama; ?>
                </td>
         </tr>
         <tr>
                <td style="width: 20%;">
                    Status
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $status; ?>
                </td>
         </tr>
         <tr>
                <td style="width: 20%;">
                  Jenis Kelamin
                </td>
                <td> : </td>
                <td style='width:70%;'>

                      <?php
                        if($jenisKelamin = "L"){
                            $namajk = "Laki-Laki";
                        }else{
                            $namajk = "Perempuan";
                        }
                      echo $namajk;
                      ?>

                </td>
         </tr>
         <tr>
                <td style="width: 20%;">Kewarganegaraan</td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $namaKewarganegaraanLain;?>
                </td>
         </tr>
         <tr>
                <td style="width: 20%;">
                  Nama Suami / Istri
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $namaSuamiIstri;?>
                </td>
          </tr>
          <tr>
                <td style="width: 20%;">
                    Jumlah Anak
                </td>
                <td> : </td>
                <td  style='width:70%;'>

                    <?php
                        echo $jumlahAnak;
                        ?>

                </td>
          </tr>
            <?php
            if($jumlahAnak > 0){
              for ($i=1; $i <= $jumlahAnak ; $i++) {
                  while($a = mysqli_fetch_assoc($res3)){
                    $namaAnak = $a['nama_anak'];
                  echo"<tr>
                      <td style='width:20%;'>
                          Nama Anak ke-".$i."
                      </td>
                      <td> : </td>
                      <td  colspan='2' style='width:70%;'>".
                            $namaAnak
                      ."</td>
                </tr>";
                $i++;
                }
                
              }
  
            }
            else{
  
            }
            ?>
          <tr>
              <td colspan="3" bgcolor="#aaa" align='center'><strong>Data Diri di Indonesia</strong></td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Alamat Rumah
              </td>
              <td> : </td>
              <td  style='width:70%;'>
                <?php echo $alamatRumahIndo;?>
              </td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Kota
              </td>
              <td> : </td>
              <td style='width:70%;'>
                <?php echo $kotaIndo;?>
              </td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Provinsi
              </td>
              <td> : </td>
              <td  style='width:70%;'>
                <?php echo $provinsiIndo;?>
              </td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Kode Pos
              </td>
              <td> : </td>
              <td   style='width:70%;'>
                <?php echo $kodePosIndo; ?>
              </td>
          </tr>
           <tr>
              <td style='width:20%;'>
                  No. Telp
              </td>
              <td> : </td>
              <td  style='width:70%;'>
                <?php echo $noTelpIndo;?>
              </td>
          </tr>
          <tr>
                <td colspan="8" bgcolor="#999" align="center">Data Diri di Thailand</td>
          </tr>
          <tr>
                  <td style="width:20%;">
                    Alamat rumah
                  </td>
                  <td> : </td>
                  <td   style="width:70%">
                    <?php echo $alamatRumahThai;?>
                </td>
         </tr>
         <tr>
                <td style="width:20%;">
                    Kode Pos
                </td>
                <td> : </td>
                <td   style='width:70%;'>
                    <?php echo $kodePosThai;?>
                </td>
          </tr>
          <tr>
                <td style='width:20%;'>
                     Kota
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $kotaThai;?>
                </td>
          </tr>
          <tr>
                <td style='width:20%;'>
                   No Telp
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $telpThai;?>
                </td>
          </tr>
          <tr>
                <td style='width:20%;'>
                  Email
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $emailUser;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Pekerjaan
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                   <?php echo $pekerjaanThai;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Nama Tempat <br/>kerja/Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $tempatKerjaSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  No Tlpn tempat <br/>kerja/Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                 <?php echo $telpTempatKerjaSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                    Alamat Kantor / sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $alamatKantorSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                    Kode Pos <br/>Kantor Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $kodePosKantorSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                    Kota Kantor Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $kotakantorSekolah;?>
                </td>
            </tr>
            <tr>
                <td>

                </td>
                <td>  </td>
                <td>

                </td>
            </tr>
            <tr>
              <td colspan="8" bgcolor="#aaa" align='center'>Informasi Paspor</td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Jenis Paspor
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $paspor;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Nomor Paspor
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $nomorPaspor;?>
                </td>
             </tr>
             <tr>
                  <td style='width:20%;'>
                    Tempat dikeluarkan
                  </td>
                  <td> : </td>
                  <td  style='width:70%;'>
                    <?php echo $tempatKeluar;?>
                  </td>
              </tr>
              <tr>
                  <td style='width:20%;'>
                    Tanggal dikeluarkan
                  </td>
                   <td> : </td>
                  <td  style='width:70%;'>
                    <?php echo $tanggalKp;?>
                  </td>
               </tr>
               <tr>
                  <td style='width:20%;'>
                    Berlaku <br/>Sampai dengan
                  </td>
                  <td> : </td>
                  <td  style='width:70%;'>
                      <?php echo $tanggalAp;?>
                  </td>
                </tr>
              <tr>
                  <td colspan="8"></td>
              </tr>
              <tr>
                <td colspan="3" bgcolor="#000" align='center'><h3 style='color:#fff;'>KONTAK YANG DAPAT DIHUBUNGI APABILA TERDAPAT SITUASI DARURAT</h3></td>
              </tr>
                <?php
                while($b = mysqli_fetch_assoc($res4)){
                  $namaLengkapDarurat = $b['namaLengkapDarurat'];
                  $statusHubunganDarurat = $b['statusHubunganDarurat'];
                  $alamatHubunganDarurat = $b['alamatHubunganDarurat'];
                  $kodePosDarurat = $b['kodePosDarurat'];
                  $kotaDarurat = $b['kota'];
                  $noTelpDarurat = $b['noTelpDarurat'];
                  $emailDarurat = $b['emailDarurat'];
                  $nosDarurat = $b['nosDarurat'];
                  $negaraDarurat = $b['negaraDarurat'];
                ?>
                <!--Looping nanti-->
              <tr>
                <td colspan="8" bgcolor="#999" style="text-transform:capitalize;">Di <?php echo $negaraDarurat;?></td>
                </tr>
              <tr>
                <td style='width:20%;'>
                  Nama Lengkap
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $namaLengkapDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Hubungan dengan pendaftar
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $statusHubunganDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Alamat <br/>(*Nama Jalan dan Nomor)
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $alamatHubunganDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Kode pos
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $kodePosDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Kota
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $kotaDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Nomor tlpn
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $noTelpDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  No Seluler
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $nosDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Alamat Email
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $emailDarurat;?>
                </td>
              </tr>
              <?php
              }
              ?>
               
                <tr>
                <td colspan="3" bgcolor="#000" align='center'><h3 style='color:#fff;'>ISIAN BAGI PENDAFTARAN ANAK DI BAWAH USIA 18 TAHUN<br/> OLEH ORANG TUA</h3></td>
                </tr>
                <?php
                while($c = mysqli_fetch_assoc($res2)){
                  $namaLengkapOrtu = $c['namaLengkapOrtu'];
                  $tempatLahirOrtu = $c['tempatLahirOrtu'];
                  $tanggalLahirOrtu = tgl_convert($c['tanggalLahirOrtu']);
                  $kewarganegaraanOrtu = $c['kewarganegaraanOrtu'];
                  $noPasporOrtu = $c['noPasporOrtu'];
                  $statusOrtu = $c['statusOrtu'];
                  $namaAnak = $c['namaAnak'];
                ?>
                <tr>
                <td colspan="8" bgcolor="#999" style="text-transform:capitalize;">Data <?php echo $statusOrtu;?></td>
                </tr>
                <tr>
                <td style='width:20%;'>
                  Nama Lengkap
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $namaLengkapOrtu;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Tempat dan Tanggal Lahir
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $tempatLahirOrtu.", ".$tanggalLahirOrtu;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Kewarganegaraan
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $kewarganegaraanOrtu;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  No. Paspor
                </td>
                <td> : </td>
                <td   style='width:70%;'>
                  <?php echo $noPasporOrtu;?>
                </td>
              </tr>
              <?php
              }
               ?>
              <tr>
                <td colspan="3" bgcolor="#000000" align='center'><h3 style='color:#ffffff;'>INFORMASI KEKONSULERAN</h3></td>
              </tr>
               <tr>
              <td style='width:20%;'>
                Tanggal ketibaan <br/> di Thailand <i><br/>(tanggal/bulan/tahun)</i>
              </td>
              <td> : </td>
              <td   style='width:70%;'>
                <?php echo $tanggalTiba;?>
              </td>
            </tr>
             <tr>
              <td style='width:20%;'>
                Lama tinggal</i>
              </td>
              <td> : </td>
              <td   style='width:70%;'>
                <?php echo $lamatinggal;?>
              </td>
            </tr>
            <tr>
                <td  style='width:20%;'>Maksud tinggal <br/> di Thailand</td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $maksudTinggal;?>
                </td>
            </tr>
            <tr>
                <td  style='width:20%;'>Tempat dan Tanggal <br/>Pengisian Formulir</td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $tempattpf.", ".$tanggalttpf;?>
                </td>
            </tr>
  </tbody>
</table>


 
<?php
}
else{
echo"data yang anda cari tidak ada";
}
?>
