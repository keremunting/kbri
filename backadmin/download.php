<?PHP 
ob_start(); // buka library laporan
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
/*error_reporting(E_ALL); */
error_reporting(0);
require_once('./tools/conf.php');  


function tgl_convert($tgl){
			$exploded = explode('-',$tgl);
			$imploded = $exploded[2].'-'.$exploded[1].'-'.$exploded[0];
			return $imploded;
}
$id = mysqli_real_escape_string($con, $_GET['ids']);
$sql1 = "SELECT
        tidentitas.id_identitas as idIdentitas,
        tidentitas.nama as nama,
        tidentitas.foto as foto,
        tidentitas.tempat_lahir as tempatLahir,
        tidentitas.tgl_lahir_user as tglLahirUser,
        tidentitas.agama as agama,
        tidentitas.status as status,
        tidentitas.jk as jenisKelamin,
        tidentitas.email as emailUser,
        tidentitas.nama_suami_istri as namaSuamiIstri,
        tidentitas.jmlanak as jumlahAnak,
        tidentitas.alamatrumahi as alamatRumahIndo,
        tidentitas.kotai as kotaIndo,
        tidentitas.provinsii as provinsiIndo,
        tidentitas.Kode_posi as kodePosIndo,
        tidentitas.no_tlpi as noTelpIndo,
        tidentitas.paspor as paspor,
        tidentitas.nomor_paspor as nomorPaspor,
        tidentitas.tempat_keluar as tempatKeluar,
        tidentitas.tanggalkp as tanggalKp,
        tidentitas.tanggalap as tanggalAp,
        tidentitas.alamat_rumaht as alamatRumahThai,
        tidentitas.Kode_post as kodePosThai,
        tidentitas.kotat as kotaThai,
        tidentitas.tlpt as telpThai,
        tidentitas.pekerjaant as pekerjaanThai,
        tidentitas.tanggal_tiba as tanggalTiba,
        tidentitas.maksudtinggal as maksudTinggal,
        tidentitas.nama_kewarganegaraan_lain as namaKewarganegaraanLain,
        tidentitas.tempattpf as tempattpf,
        tidentitas.tanggalttpf as tanggalttpf,
        tidentitas.lamatinggal as lamatinggal,

        tempat_kerja_kuliah.tmptkerjasekolah as tempatKerjaSekolah,
        tempat_kerja_kuliah.tlptmptkerjasekolah as telpTempatKerjaSekolah,
        tempat_kerja_kuliah.alamatkantorsekolah as alamatKantorSekolah,
        tempat_kerja_kuliah.Kodeposkantorsekolah as kodePosKantorSekolah,
        tempat_kerja_kuliah.kotakantorsekolah as kotakantorSekolah
        FROM tidentitas
        JOIN tempat_kerja_kuliah ON tempat_kerja_kuliah.id_identitas = tidentitas.id_identitas
        WHERE tidentitas.id_identitas = $id GROUP BY tidentitas.id_identitas";
  $sql2 = "SELECT nama_lengkap as namaLengkapOrtu, tempat_lahir as tempatLahirOrtu,
          tgl_lahir as tanggalLahirOrtu, kewarganegaraan as kewarganegaraanOrtu,
          no_paspor as noPasporOrtu, status as status
          FROM torang_tua
          WHERE id_identitas = $id";
  $sql3 = "SELECT nama_anak FROM tanak WHERE id_identitas = $id";
  $sql4 = "SELECT
          namalengkap as namaLengkapDarurat,
          statushubungan as statusHubunganDarurat,
          alamathubungan as alamatHubunganDarurat,
          Kodepos as kodePosDarurat,
          kota,
          notlp as noTelpDarurat, nos as nosDarurat, email as emailDarurat,
          negara as negaraDarurat
          FROM table_kontak_darurat
          WHERE id_identitas = $id";
  $res1 = mysqli_query($con,$sql1);
  if (mysqli_num_rows($res1) > 0)     
{  
  $res2 = mysqli_query($con,$sql2);
  $res3 = mysqli_query($con,$sql3);
  $res4 = mysqli_query($con,$sql4);

  $d = mysqli_fetch_assoc($res1);
    $idIdentitas  = $d['idIdentitas'];
    $nama = $d['nama'];
    $foto = $d['foto'];
    $tempatLahir = $d['tempatLahir'];
    $tglLahirUser = tgl_convert($d['tglLahirUser']);
    $agama = $d['agama'];
    $status = $d['status'];
    $jenisKelamin = $d['jenisKelamin'];
    $emailUser = $d['emailUser'];
    $namaSuamiIstri = $d['namaSuamiIstri'];
    $jumlahAnak = $d['jumlahAnak'];
    $alamatRumahIndo = $d['alamatRumahIndo'];
    $kotaIndo = $d['kotaIndo'];
    $provinsiIndo = $d['provinsiIndo'];
    $kodePosIndo = $d['kodePosIndo'];
    $noTelpIndo = $d['noTelpIndo'];
    $paspor = $d['paspor'];
    $nomorPaspor = $d['nomorPaspor'];
    $tempatKeluar = $d['tempatKeluar'];
    $tanggalKp = tgl_convert($d['tanggalKp']);
    $tanggalAp = tgl_convert($d['tanggalAp']);
    $alamatRumahThai = $d['alamatRumahThai'];
    $kodePosThai = $d['kodePosThai'];
    $kotaThai = $d['kotaThai'];
    $telpThai = $d['telpThai'];
    $pekerjaanThai = $d['pekerjaanThai'];
    $tanggalTiba = tgl_convert($d['tanggalTiba']);
    $maksudTinggal = $d['maksudTinggal'];
    $namaKewarganegaraanLain = $d['namaKewarganegaraanLain'];
    $tempattpf = $d['tempattpf'];
    $tanggalttpf = tgl_convert($d['tanggalttpf']);

    $tempatKerjaSekolah = $d['tempatKerjaSekolah'];
    $telpTempatKerjaSekolah = $d['telpTempatKerjaSekolah'];
    $alamatKantorSekolah = $d['alamatKantorSekolah'];
    $kodePosKantorSekolah = $d['kodePosKantorSekolah'];
    $kotakantorSekolah = $d['kotakantorSekolah'];
    $lamatinggal = tgl_convert($d['lamatinggal']);
?>
<style>
.header {border-bottom:solid 3px #EF4135; height:85px; width:90%; margin:auto; margin-bottom:20px;}
.header img {width:50px!important;height:30px!important; float:left; margin-right:10px;}
.header h3{font-family:Times, serif;font-size:30px; line-height:30px; text-align:center; margin-top:20px; font-weight:bold; text-transform:uppercase}
.header p {text-align:center; font-weight:bold; margin:auto;padding:1px!important;}
.header span {padding-top:10px;}
.table {border: solid 0px #888888; width:90%;margin:auto;text-align:left;}
.table th {border-right:0px #999999; background:#EF4135;color:#ffffff;font-size:16px; padding:5px;text-align:left;
text-transform:uppercase}
.table td {border-bottom: 1px #aaaaaa; border-bottom:solid 0px #aaaaaa; padding:8px; word-break:break-all;
font-family:Arial, Helvetica, sans-serif;font-size:15px;}
p {margin:0px;padding:4px!important; font-size:15px;font-family:Arial, Helvetica, sans-serif;text-transform:capitalize}
.ttd {margin-top:50px; line-height:25px;} 
</style>

<div class="header"><img src="../img/garuda-perwakilan2.png" /><span>
<h3>TANDA BUKTI LAPOR DIRI ONLINE 2016</h3>
</span>

</div>
   <table cellpadding="10" cellspacing="10" class="table">
        <tbody>
        <tr>
       
                <td colspan="3" bgcolor="#999999" align="center"><h3>DATA IDENTITAS PELAPOR</h3></td>
        </tr>
        <tr>
                <td style="width: 20%;">
                    Nomor Registrasi
                </td>
                <td style="width: 10%;"> :  </td>
                <td style="width:70%;">
                  <?php echo $id;?>
                </td>
        </tr>
        <tr>
                <td style="width: 20%;">
                    Nama Lengkap
                </td>
                <td style="width: 10%;"> :  </td>
                <td style="width:70%;">
                  <?php echo $nama;?>
                </td>
        </tr>
         <tr>
              <td style="width: 20%;"> FOTO
                </td>
                <td> :  </td>
                <td style="width:70%;">
                            <?php echo "<img src='../images/$foto' width='150' height='180'>"; ?>
                </td>

        </tr>
        <tr>
                <td style="width: 20%;">
                    Tempat Lahir / Tanggal lahir
                  </td>
                  <td> : </td>
                  <td style='width:70%;'>
                    <?php echo $tempatLahir;?> / <?php echo $tglLahirUser;?>
                </td>
        </tr>
        <tr>
                <td style="width: 20%;">
                  Agama
                </td>
                <td> : </td>
                <td style='width:70%;'>
                  <?php echo $agama; ?>
                </td>
         </tr>
         <tr>
                <td style="width: 20%;">
                    Status
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $status; ?>
                </td>
         </tr>
         <tr>
                <td style="width: 20%;">
                  Jenis Kelamin
                </td>
                <td> : </td>
                <td style='width:70%;'>

                      <?php
                        if($jenisKelamin = "L"){
                            $namajk = "Laki-Laki";
                        }else{
                            $namajk = "Perempuan";
                        }
                      echo $namajk;
                      ?>

                </td>
         </tr>
         <tr>
                <td style="width: 20%;">Kewarganegaraan</td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $namaKewarganegaraanLain;?>
                </td>
         </tr>
         <tr>
                <td style="width: 20%;">
                  Nama Suami / Istri
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $namaSuamiIstri;?>
                </td>
          </tr>
          <tr>
                <td style="width: 20%;">
                    Jumlah Anak
                </td>
                <td> : </td>
                <td  style='width:70%;'>

                    <?php
                        echo $jumlahAnak;
                        ?>

                </td>
          </tr>
         <?php
            if($jumlahAnak > 0){
              $i=1;
                  while($a = mysqli_fetch_array($res3)){
                    $namaAnak = $a['nama_anak'];
                  echo"<tr><td style='width:20%;'>Nama Anak ke-".$i."</td>
                      <td> : </td>
                      <td style='width:70%;'>".$namaAnak."</td>
                      </tr>";
                     $i++;
              	  }
            }
            else{
  
            }
            ?>  
         <tr>
              <td colspan="3" bgcolor="#aaa" align='center'><strong>Data Diri di Indonesia</strong></td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Alamat Rumah
              </td>
              <td> : </td>
              <td  style='width:70%;'>
                <?php echo $alamatRumahIndo;?>
              </td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Kota
              </td>
              <td> : </td>
              <td style='width:70%;'>
                <?php echo $kotaIndo;?>
              </td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Provinsi
              </td>
              <td> : </td>
              <td  style='width:70%;'>
                <?php echo $provinsiIndo;?>
              </td>
          </tr>
          <tr>
              <td style='width:20%;'>
                  Kode Pos
              </td>
              <td> : </td>
              <td   style='width:70%;'>
                <?php echo $kodePosIndo; ?>
              </td>
          </tr>
           <tr>
              <td style='width:20%;'>
                  No. Telp
              </td>
              <td> : </td>
              <td  style='width:70%;'>
                <?php echo $noTelpIndo;?>
              </td>
          </tr>
          <tr>
                <td colspan="3" bgcolor="#999999" align="center">Data Diri di Thailand</td>
          </tr>
          <tr>
                  <td style="width:20%;">
                    Alamat rumah
                  </td>
                  <td> : </td>
                  <td   style="width:70%">
                    <?php echo $alamatRumahThai; ?>
                </td>
         </tr>
         <tr>
                <td style="width:20%;">
                    Kode Pos
                </td>
                <td> : </td>
                <td   style='width:70%;'>
                    <?php echo $kodePosThai;?>
                </td>
          </tr>
          <tr>
                <td style='width:20%;'>
                     Kota
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $kotaThai;?>
                </td>
          </tr>
          <tr>
                <td style='width:20%;'>
                   No Telp
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $telpThai;?>
                </td>
          </tr>
          <tr>
                <td style='width:20%;'>
                  Email
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $emailUser;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Pekerjaan
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                   <?php echo $pekerjaanThai;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Nama Tempat <br/>kerja/Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $tempatKerjaSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  No Tlpn tempat <br/>kerja/Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                 <?php echo $telpTempatKerjaSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                    Alamat Kantor / sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $alamatKantorSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                    Kode Pos <br/>Kantor Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $kodePosKantorSekolah;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                    Kota Kantor Sekolah
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $kotakantorSekolah;?>
                </td>
            </tr>
            <tr>
                <td>

                </td>
                <td>  </td>
                <td>

                </td>
            </tr>
            <tr>
              <td colspan="3" bgcolor="#aaa" align='center'>Informasi Paspor</td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Jenis Paspor
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $paspor;?>
                </td>
            </tr>
            <tr>
                <td style='width:20%;'>
                  Nomor Paspor
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $nomorPaspor;?>
                </td>
             </tr>
             <tr>
                  <td style='width:20%;'>
                    Tempat dikeluarkan
                  </td>
                  <td> : </td>
                  <td  style='width:70%;'>
                    <?php echo $tempatKeluar;?>
                  </td>
              </tr>
              <tr>
                  <td style='width:20%;'>
                    Tanggal dikeluarkan
                  </td>
                   <td> : </td>
                  <td  style='width:70%;'>
                    <?php echo $tanggalKp;?>
                  </td>
               </tr>
               <tr>
                  <td style='width:20%;'>
                    Berlaku <br/>Sampai dengan
                  </td>
                  <td> : </td>
                  <td  style='width:70%;'>
                      <?php echo $tanggalAp;?>
                  </td>
                </tr>
                <tr>
                <td colspan="3" bgcolor="#999999" align='center'><h3>KONTAK YANG DAPAT DIHUBUNGI APABILA TERDAPAT SITUASI DARURAT</h3></td>
              </tr>
                <?php
                while($b = mysqli_fetch_assoc($res4)){
                  $namaLengkapDarurat = $b['namaLengkapDarurat'];
                  $statusHubunganDarurat = $b['statusHubunganDarurat'];
                  $alamatHubunganDarurat = $b['alamatHubunganDarurat'];
                  $kodePosDarurat = $b['kodePosDarurat'];
                  $kotaDarurat = $b['kota'];
                  $noTelpDarurat = $b['noTelpDarurat'];
                  $emailDarurat = $b['emailDarurat'];
                  $nosDarurat = $b['nosDarurat'];
                  $negaraDarurat = $b['negaraDarurat'];
                ?>
                <!--Looping nanti-->
              <tr>
                <td colspan="3" bgcolor="#aaaaaa" style="text-transform:capitalize;">Di <?php echo $negaraDarurat;?></td>
                </tr>
              <tr>
                <td style='width:20%;'>
                  Nama Lengkap
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $namaLengkapDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Hubungan dengan pendaftar
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $statusHubunganDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Alamat <br/>(*Nama Jalan dan Nomor)
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $alamatHubunganDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Kode pos
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $kodePosDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Kota
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $kotaDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Nomor tlpn
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $noTelpDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  No Seluler
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $nosDarurat;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Alamat Email
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $emailDarurat;?>
                </td>
              </tr>
              <?php
              }
              ?>
              <tr>
                <td colspan="3" bgcolor="#999999" align='center'><h3>ISIAN BAGI PENDAFTARAN ANAK DI BAWAH USIA 18 TAHUN<br/> OLEH ORANG TUA</h3></td>
                </tr>
                <?php
                while($c = mysqli_fetch_assoc($res2)){
                  $namaLengkapOrtu = $c['namaLengkapOrtu'];
                  $tempatLahirOrtu = $c['tempatLahirOrtu'];
                  $tanggalLahirOrtu = tgl_convert($c['tanggalLahirOrtu']);
                  $kewarganegaraanOrtu = $c['kewarganegaraanOrtu'];
                  $noPasporOrtu = $c['noPasporOrtu'];
                  $statusOrtu = $c['status'];
                  $namaAnak = $c['namaAnak'];
                ?>
                <tr>
                <td colspan="3" bgcolor="#999999" style="text-transform:capitalize;">Data <?php echo $statusOrtu;?></td>
                </tr>
                <tr>
                <td style='width:20%;'>
                  Nama Lengkap
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $namaLengkapOrtu;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Tempat dan Tanggal Lahir
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $tempatLahirOrtu.", ".$tanggalLahirOrtu;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  Kewarganegaraan
                </td>
                <td> : </td>
                <td  style='width:70%;'>
                  <?php echo $kewarganegaraanOrtu;?>
                </td>
              </tr>
              <tr>
                <td style='width:20%;'>
                  No. Paspor
                </td>
                <td> : </td>
                <td   style='width:70%;'>
                  <?php echo $noPasporOrtu;?>
                </td>
              </tr>
              <?php
              }
               ?>
               <tr>
                <td colspan="3" bgcolor="#999999" align='center'><h3>INFORMASI KEKONSULERAN</h3></td>
              </tr>
              <tr>
	              <td style='width:20%;'>
	                Tanggal ketibaan di Thailand 
	              </td>
	              <td> : </td>
	              <td   style='width:70%;'>
	                <?php echo $tanggalTiba;?>
	              </td>
            </tr>
             <tr>
	              <td style='width:20%;'>
	                Lama tinggal
	              </td>
	              <td> : </td>
	              <td   style='width:70%;'>
	                <?php echo $lamatinggal;?>
	              </td>
            </tr>
            <tr>
                <td  style='width:20%;'>Maksud tinggal <br/> di Thailand</td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $maksudTinggal;?>
                </td>
            </tr>
            <tr>
                <td  style='width:20%;'>Tempat dan Tanggal <br/>Pengisian Formulir</td>
                <td> : </td>
                <td  style='width:70%;'>
                    <?php echo $tempattpf.", ".$tanggalttpf;?>
                </td>
            </tr>
       </tbody>
     </table>


 
<?php
}
else{
echo"data yang anda cari tidak ada";
}



$content = ob_get_clean(); 
// conversion HTML => PDF
require_once('../html2pdf/html2pdf.class.php');


$html2pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
$content_PDF = $html2pdf->Output('hasil-laporan.pdf','D');//output laporan setelah di download



?>