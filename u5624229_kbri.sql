-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 30, 2016 at 04:42 PM
-- Server version: 5.5.47-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u5624229_kbri`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_kontak_darurat`
--

CREATE TABLE IF NOT EXISTS `table_kontak_darurat` (
  `id_kontak_d` int(11) NOT NULL AUTO_INCREMENT,
  `id_identitas` int(5) unsigned zerofill NOT NULL,
  `namalengkap` varchar(200) NOT NULL,
  `statushubungan` varchar(200) NOT NULL,
  `alamathubungan` varchar(200) NOT NULL,
  `Kodepos` varchar(10) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `notlp` varchar(18) NOT NULL,
  `nos` varchar(18) NOT NULL,
  `email` varchar(200) NOT NULL,
  `negara` enum('thailand','indonesia') NOT NULL,
  PRIMARY KEY (`id_kontak_d`),
  KEY `id_identitas` (`id_identitas`),
  KEY `id_identitas_2` (`id_identitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `table_kontak_darurat`
--

INSERT INTO `table_kontak_darurat` (`id_kontak_d`, `id_identitas`, `namalengkap`, `statushubungan`, `alamathubungan`, `Kodepos`, `kota`, `notlp`, `nos`, `email`, `negara`) VALUES
(1, 00018, 'dinda', 'saudara', 'alamat dinda', '0819282', 'jakarta', '081912912129', '019218212891', 'dinda@mail.com', 'indonesia'),
(2, 00018, 'dani', 'teman', 'dani alamat', '081912', 'thailand', '0819283283', '0129238123', 'dani@mail.com', 'thailand'),
(3, 00019, 'hubungan nama iandonesia', 'saudara', 'alamat nama jalan indonesia', '0819', 'jakarta', '01281928121', '01928121281', 'namaindi@gmail.com', 'indonesia'),
(4, 00019, 'rini', 'teman', 'alamat jan nomorrini', '0921', 'bangkok', '09129212', '901291221', 'rini@mail.com', 'thailand'),
(5, 00020, 'asniar', 'ibu', 'manggar', '103660', 'manggar', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'indonesia'),
(6, 00020, 'haerul imam', 'saudara', 'bangkok', '33472', 'bangkok', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'thailand'),
(7, 00021, 'asniar', 'ibu', 'manggar', '103660', 'manggar', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'indonesia'),
(8, 00021, 'Ramadhan', 'saudara', 'bangkok', '33472', 'bangkok', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'thailand'),
(9, 00022, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(10, 00022, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(11, 00023, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(12, 00023, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(13, 00024, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(14, 00024, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(15, 00025, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(16, 00025, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(17, 00026, '', '', '', '', '', '', '', '', 'indonesia'),
(18, 00026, '', '', '', '', '', '', '', '', 'thailand'),
(19, 00027, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(20, 00027, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(21, 00028, 'asniar', 'ibu', 'manggar', '103660', 'manggar', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'indonesia'),
(22, 00028, 'Ramadhan', 'saudara', 'bangkok', '33472', 'bangkok', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'thailand'),
(23, 00029, 'asniar', 'ibu', 'manggar', '103660', 'manggar', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'indonesia'),
(24, 00029, 'Ramadhan', 'saudara', 'bangkok', '33472', 'bangkok', '0620819073', '0620819073', 'jokogunawan2015@gmail.com', 'thailand'),
(25, 00030, 'asdj', 'asidiasdn', 'oasdasdk', '93218', 'sadj', '98234832', '12391283', 'asda@sdasj.cm', 'indonesia'),
(26, 00030, 'arief', 'sadu', 'asdk', '2130', 'jsak', '01293823', '09128312', 'asdasd@sada.com', 'thailand'),
(27, 00031, '', '', '', '', '', '', '', '', 'indonesia'),
(28, 00031, '', '', '', '', '', '', '', '', 'thailand'),
(29, 00032, '', '', '', '', '', '', '', '', 'indonesia'),
(30, 00032, '', '', '', '', '', '', '', '', 'thailand'),
(31, 00033, 'asdasd', 'ssadasd', 'sasdasd', '2223423', 'asdasdads', '34234234324', '423423432', 'asdad@sdad', 'indonesia'),
(32, 00033, 'dasda', 'asdasd', 'asdadas', '12313', 'sadasd', '432423423', '123123123', 'aads@asda', 'thailand'),
(33, 00034, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(34, 00034, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(35, 00035, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(36, 00035, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(37, 00036, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(38, 00036, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(39, 00037, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(40, 00037, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(41, 00038, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(42, 00038, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(43, 00039, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(44, 00039, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(45, 00040, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(46, 00040, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(47, 00041, 'ian', 'teman', 'surabaya alamat', '023', 'surabaya', '01929832129', '0129231230', 'ian@gmail.com', 'indonesia'),
(48, 00041, 'lukman', 'teman', 'alamat lukman', '021', 'bangkok', '08192120', '091283139', 'lukmain@mail.com', 'thailand'),
(49, 00042, 'Arief', 'Saudara', 'Jl. Dimana', '124889', 'Bandung', '08236475819', '081264531998', 'ada@dasdasd.com', 'indonesia'),
(50, 00042, 'Nam Lui Pat', 'Saudara', 'Jl. Pat Liu', '9008765', 'Bangkok', '091874653218', '035417493521', 'aaaa@dassda', 'thailand');

-- --------------------------------------------------------

--
-- Table structure for table `tanak`
--

CREATE TABLE IF NOT EXISTS `tanak` (
  `id_anak` int(11) NOT NULL AUTO_INCREMENT,
  `id_identitas` int(5) unsigned zerofill NOT NULL,
  `nama_anak` varchar(200) NOT NULL,
  PRIMARY KEY (`id_anak`),
  KEY `id_identitas` (`id_identitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tanak`
--

INSERT INTO `tanak` (`id_anak`, `id_identitas`, `nama_anak`) VALUES
(1, 00019, 'arif'),
(2, 00019, 'monic'),
(3, 00022, 'arif'),
(4, 00022, 'monic'),
(5, 00023, ''),
(6, 00023, ''),
(7, 00024, ''),
(8, 00024, ''),
(9, 00025, ''),
(10, 00025, ''),
(11, 00027, ''),
(12, 00027, ''),
(13, 00034, ''),
(14, 00034, ''),
(15, 00035, ''),
(16, 00035, ''),
(17, 00036, ''),
(18, 00036, ''),
(19, 00037, ''),
(20, 00037, ''),
(21, 00038, ''),
(22, 00038, ''),
(23, 00039, ''),
(24, 00039, ''),
(25, 00040, ''),
(26, 00040, ''),
(27, 00041, ''),
(28, 00041, '');

-- --------------------------------------------------------

--
-- Table structure for table `tempat_kerja_kuliah`
--

CREATE TABLE IF NOT EXISTS `tempat_kerja_kuliah` (
  `id_tempat_kerja_kuliah` int(11) NOT NULL AUTO_INCREMENT,
  `id_identitas` int(5) unsigned zerofill NOT NULL,
  `tmptkerjasekolah` varchar(200) NOT NULL,
  `tlptmptkerjasekolah` varchar(200) NOT NULL,
  `alamatkantorsekolah` varchar(200) NOT NULL,
  `Kodeposkantorsekolah` varchar(10) NOT NULL,
  `kotakantorsekolah` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tempat_kerja_kuliah`),
  KEY `id_identitas` (`id_identitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tempat_kerja_kuliah`
--

INSERT INTO `tempat_kerja_kuliah` (`id_tempat_kerja_kuliah`, `id_identitas`, `tmptkerjasekolah`, `tlptmptkerjasekolah`, `alamatkantorsekolah`, `Kodeposkantorsekolah`, `kotakantorsekolah`) VALUES
(1, 00018, 'kampus bangkok', '081928382328', 'bangkok tengah', '0819281', 'bangkok kantor'),
(2, 00019, 'nama tempat kerja', '00910281218', 'alamat empat kerja', '0812', 'kota tempat kerja'),
(3, 00020, 'chulalongkorn', '464564', 'bangkok', 'sdgds', 'sgdsd'),
(4, 00021, 'chulalongkorn', '464564', 'bangkok', 'sdgds', 'sgdsd'),
(5, 00022, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(6, 00023, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(7, 00024, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(8, 00025, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(9, 00026, '', '', '', '', ''),
(10, 00027, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(11, 00028, 'chulalongkorn', '464564', 'bangkok', 'sdgds', 'sgdsd'),
(12, 00029, 'chulalongkorn', '464564', 'bangkok', 'sdgds', 'sgdsd'),
(13, 00030, 'bandung', '0822192822', 'bandung', '0129283', 'badung'),
(14, 00031, '', '', '', '', ''),
(15, 00032, '', '', '', '', ''),
(16, 00033, 'sdasadsasd', '324324324324', 'asdaddas', 'adasdas', 'dasdasdasdasd'),
(17, 00034, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(18, 00035, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(19, 00036, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(20, 00037, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(21, 00038, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(22, 00039, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(23, 00040, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(24, 00041, 'kampus bangkok', '093842342439', 'jalan bangkok', '0192812', 'bangkok'),
(25, 00042, 'PT. Swasta', '092378906412', 'Jl. Ngarau', '9975834', 'Kelupaen');

-- --------------------------------------------------------

--
-- Table structure for table `tidentitas`
--

CREATE TABLE IF NOT EXISTS `tidentitas` (
  `id_identitas` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `tempat_lahir` varchar(200) NOT NULL,
  `tgl_lahir_user` date NOT NULL,
  `agama` varchar(12) NOT NULL,
  `status` enum('menikah','Belum menikah','cerai') NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `email` varchar(200) NOT NULL,
  `nama_suami_istri` varchar(100) DEFAULT NULL,
  `jmlanak` int(11) DEFAULT NULL,
  `alamatrumahi` varchar(200) NOT NULL,
  `kotai` varchar(100) NOT NULL,
  `provinsii` varchar(100) NOT NULL,
  `Kode_posi` varchar(10) NOT NULL,
  `no_tlpi` varchar(18) DEFAULT NULL,
  `paspor` enum('biasa','dinas','diplomatik') NOT NULL,
  `nomor_paspor` varchar(100) DEFAULT NULL,
  `tempat_keluar` varchar(200) DEFAULT NULL,
  `tanggalkp` date DEFAULT NULL,
  `tanggalap` date DEFAULT NULL,
  `alamat_rumaht` varchar(200) NOT NULL,
  `Kode_post` varchar(10) NOT NULL,
  `kotat` varchar(100) NOT NULL,
  `tlpt` varchar(18) NOT NULL,
  `pekerjaant` varchar(200) NOT NULL,
  `tanggal_tiba` date NOT NULL,
  `lamatinggal` date NOT NULL,
  `maksudtinggal` varchar(150) NOT NULL,
  `nama_kewarganegaraan_lain` varchar(200) DEFAULT NULL,
  `tempattpf` varchar(200) NOT NULL,
  `tanggalttpf` date NOT NULL,
  PRIMARY KEY (`id_identitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `tidentitas`
--

INSERT INTO `tidentitas` (`id_identitas`, `nama`, `foto`, `tempat_lahir`, `tgl_lahir_user`, `agama`, `status`, `jk`, `email`, `nama_suami_istri`, `jmlanak`, `alamatrumahi`, `kotai`, `provinsii`, `Kode_posi`, `no_tlpi`, `paspor`, `nomor_paspor`, `tempat_keluar`, `tanggalkp`, `tanggalap`, `alamat_rumaht`, `Kode_post`, `kotat`, `tlpt`, `pekerjaant`, `tanggal_tiba`, `lamatinggal`, `maksudtinggal`, `nama_kewarganegaraan_lain`, `tempattpf`, `tanggalttpf`) VALUES
(00018, 'jack', '145944xinsert-logo-here.jpg', 'jakarta', '2016-01-14', 'Islam', 'Belum menikah', 'L', 'apriadikoneksi@gmail.com', '', 0, 'jakarta', 'jakarta', 'jakarta', '021', '0819283737', 'biasa', '081929832329', 'jakarta', '2016-01-04', '2016-01-21', 'thailand', '9024824', 'thailand', '08129212129', 'mahasiswa', '2016-01-15', '0000-00-00', 'bekerja,sekolah,liburan,', '', 'jakarta', '2016-01-27'),
(00019, 'dana', '419931xinsert-logo-here.jpg', 'manggar', '2016-01-06', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'indri', 2, 'alamat indonesia', 'jakarta', 'jakarta', '091221', '08192833838', 'biasa', '0819281298', 'jakarta', '2016-01-08', '2016-01-21', 'alamat bangkok', '0819', 'bangkok', '0819281219', 'mahasiswa', '2016-01-06', '0000-00-00', 'liburan,', 'malaysia', 'jakarta', '2016-01-27'),
(00020, 'joko gunawan', '223783Berita 2012-Logo garuda.JPG', 'tanjungpandan', '1989-01-26', 'Islam', 'Belum menikah', 'L', 'jokogunawan2015@gmail.com', '', 0, 'manggar', 'manggar', 'belitung timur', '33472', '0888460075', 'biasa', 'A4363325', 'Bandung', '2013-01-06', '2019-01-06', 'manggar', '103360', 'bangkok', '0888460075', 'mahasiswa', '2016-01-08', '0000-00-00', 'sekolah,', '', 'bangkok', '2016-01-11'),
(00021, 'joko gunawan', '809951Berita 2012-Logo garuda.JPG', 'tanjungpandan', '1988-01-05', 'Islam', 'Belum menikah', 'L', 'jokogunawan2015@gmail.com', '', 0, 'manggar', 'manggar', 'belitung timur', '33472', '0888460075', 'biasa', 'A4363325', 'Bandung', '2013-01-02', '2019-01-12', 'manggar', '103360', 'bangkok', '0888460075', 'mahasiswa', '2016-01-06', '0000-00-00', 'sekolah,', '', 'bangkok', '2016-01-27'),
(00022, 'andi', '438172xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00023, 'andi', '599910xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00024, 'andi', '294344xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00025, 'andi', '931298xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00026, '', 'no_image.jpg', '', '0000-00-00', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '0000-00-00', '0000-00-00', ',', '', '', '0000-00-00'),
(00027, 'andi', '738435xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00028, 'joko1', '945734Berita 2012-Logo garuda.JPG', 'tanjungpandan', '2016-01-07', 'Islam', 'Belum menikah', 'L', 'jokogunawan2015@gmail.com', '', 0, 'manggar', 'manggar', 'belitung timur', '33472', '0888460075', 'biasa', 'A4363325', 'Bandung', '2013-01-06', '2019-01-04', 'manggar', '103360', 'bangkok', '0888460075', 'mahasiswa', '2016-01-15', '0000-00-00', 'sekolah,', '', 'bangkok', '2016-01-05'),
(00029, 'joko2', '453709Berita 2012-Logo garuda.JPG', 'tanjungpandan', '1987-01-08', 'Islam', 'Belum menikah', 'L', 'jokogunawan2015@gmail.com', '', 0, 'manggar', 'manggar', 'belitung timur', '33472', '0888460075', 'biasa', 'A4363325', 'Bandung', '2013-01-07', '2019-01-05', 'manggar', '103360', 'bangkok', '0888460075', 'mahasiswa', '2016-01-15', '2016-01-16', 'sekolah,', '', 'bangkok', '2016-01-28'),
(00030, 'arief', '592211sendal jepit.jpg', 'sadasd', '2016-01-04', 'Islam', 'Belum menikah', 'L', 'ariefconnect@gmail.com', '', 0, 'adasdsad', 'askdakdm', 'amsdkasdm', '03123', '03423492349', 'biasa', '098812292', 'Bandung', '2016-01-19', '2016-01-31', 'Bandung', '123123', 'Bandung', '082176901705', 'karyawan', '2016-01-16', '0000-00-00', 'bekerja,sekolah,', 'asdad', 'asdasd', '2016-01-20'),
(00031, '', 'no_image.jpg', '', '0000-00-00', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '0000-00-00', '0000-00-00', ',', '', '', '0000-00-00'),
(00032, '', 'no_image.jpg', '', '0000-00-00', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '0000-00-00', '0000-00-00', ',', '', '', '0000-00-00'),
(00033, 'asdasd', '22092069.png', 'dasdas', '2016-01-04', 'Islam', 'Belum menikah', 'L', 'ariefconnect@gmail.com', '', 0, 'asdasdas', 'asdasdas', 'asdasdasd', '231123', '3423432', 'biasa', 'dadasd', 'adaasd', '2016-01-27', '2016-01-31', 'asdasd', '23123', '213123', '21312312', 'sadasdsad', '2016-01-31', '0000-00-00', 'bekerja,sekolah,', 'asdasd', 'asdasd', '2016-01-31'),
(00034, 'andi', '271174xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00035, 'andi', '886611xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00036, 'andi', '86731xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00037, 'andi', '366955xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00038, 'andi', '447109xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00039, 'andi', '748062xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00040, 'andi', '173352xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00041, 'andi', '491509xinsert-logo-here.jpg', 'jakarta', '1990-01-07', 'Islam', 'menikah', 'L', 'apriadikoneksi@gmail.com', 'raisa', 2, 'alamat indo', 'jakarta', 'jakarta', '021', '08192837272', 'biasa', '02934823492423843', 'jakarta', '2016-01-08', '2016-01-21', 'alamt thailand', '021', 'bangkok', '0918212288', 'mahasiswa', '2016-01-08', '2016-01-29', 'sekolah,menetap,menikah', 'France', 'jakarta', '2016-01-07'),
(00042, 'Bang Arief Ganteng', 'no_image.jpg', 'Badau', '1998-01-06', 'Islam', 'Belum menikah', 'L', 'ariefconnect@gmail.com', '', 0, 'Jl. Buruh', 'Bandung', 'Jawa Barat', '0521880', '089379381839579', 'biasa', '78678679', 'Indonesia', '2016-01-13', '2020-01-13', 'jajkajdad', '0421756', 'Bandung', '081890387654', 'Buruh Harian', '2016-01-02', '2025-01-31', 'bekerja,', 'Indonesa', 'Bangkok', '2016-01-04');

-- --------------------------------------------------------

--
-- Table structure for table `torang_tua`
--

CREATE TABLE IF NOT EXISTS `torang_tua` (
  `id_orang_tua` int(11) NOT NULL AUTO_INCREMENT,
  `id_identitas` int(5) unsigned zerofill NOT NULL,
  `nama_lengkap` varchar(200) NOT NULL,
  `tempat_lahir` varchar(200) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kewarganegaraan` varchar(200) NOT NULL,
  `no_paspor` varchar(100) NOT NULL,
  `status` enum('ayah','ibu') NOT NULL,
  PRIMARY KEY (`id_orang_tua`),
  KEY `id_identitas` (`id_identitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `torang_tua`
--

INSERT INTO `torang_tua` (`id_orang_tua`, `id_identitas`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `kewarganegaraan`, `no_paspor`, `status`) VALUES
(1, 00018, 'ayah', '0', '2016-01-08', 'indonesia', '092394823424', 'ayah'),
(2, 00018, 'ibu', '0', '2016-01-08', 'indonesia', '90838234203948', 'ibu'),
(3, 00019, 'ayah', 'jakarta', '2016-01-05', 'indonesia', '081912129', 'ayah'),
(4, 00019, 'ibu', 'jakarta', '2016-01-20', 'indonesia', '092031283239', 'ibu'),
(5, 00020, '', '', '2016-01-05', '', '', 'ayah'),
(6, 00020, '', '', '2016-01-04', '', '', 'ibu'),
(7, 00021, '', '', '0000-00-00', '', '', 'ayah'),
(8, 00021, '', '', '0000-00-00', '', '', 'ibu'),
(9, 00022, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(10, 00022, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(11, 00023, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(12, 00023, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(13, 00024, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(14, 00024, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(15, 00025, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(16, 00025, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(17, 00026, '', '', '0000-00-00', '', '', 'ayah'),
(18, 00026, '', '', '0000-00-00', '', '', 'ibu'),
(19, 00027, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(20, 00027, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(21, 00028, '', '', '0000-00-00', '', '', 'ayah'),
(22, 00028, '', '', '0000-00-00', '', '', 'ibu'),
(23, 00029, '', '', '0000-00-00', '', '', 'ayah'),
(24, 00029, '', '', '0000-00-00', '', '', 'ibu'),
(25, 00030, '', '', '0000-00-00', '', '', 'ayah'),
(26, 00030, '', '', '0000-00-00', '', '', 'ibu'),
(27, 00031, '', '', '0000-00-00', '', '', 'ayah'),
(28, 00031, '', '', '0000-00-00', '', '', 'ibu'),
(29, 00032, '', '', '0000-00-00', '', '', 'ayah'),
(30, 00032, '', '', '0000-00-00', '', '', 'ibu'),
(31, 00033, '', '', '0000-00-00', '', '', 'ayah'),
(32, 00033, '', '', '0000-00-00', '', '', 'ibu'),
(33, 00034, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(34, 00034, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(35, 00035, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(36, 00035, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(37, 00036, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(38, 00036, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(39, 00037, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(40, 00037, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(41, 00038, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(42, 00038, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(43, 00039, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(44, 00039, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(45, 00040, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(46, 00040, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(47, 00041, 'nama ayah ', 'jakarta', '2016-01-06', 'indonesia', '081929102120', 'ayah'),
(48, 00041, 'nama ibu', 'jakarta', '2016-01-13', 'indonesia', '02932834234309', 'ibu'),
(49, 00042, '', '', '0000-00-00', '', '', 'ayah'),
(50, 00042, '', '', '0000-00-00', '', '', 'ibu');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `table_kontak_darurat`
--
ALTER TABLE `table_kontak_darurat`
  ADD CONSTRAINT `table_kontak_darurat_ibfk_1` FOREIGN KEY (`id_identitas`) REFERENCES `tidentitas` (`id_identitas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tanak`
--
ALTER TABLE `tanak`
  ADD CONSTRAINT `tanak_ibfk_1` FOREIGN KEY (`id_identitas`) REFERENCES `tidentitas` (`id_identitas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tempat_kerja_kuliah`
--
ALTER TABLE `tempat_kerja_kuliah`
  ADD CONSTRAINT `tempat_kerja_kuliah_ibfk_1` FOREIGN KEY (`id_identitas`) REFERENCES `tidentitas` (`id_identitas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `torang_tua`
--
ALTER TABLE `torang_tua`
  ADD CONSTRAINT `torang_tua_ibfk_1` FOREIGN KEY (`id_identitas`) REFERENCES `tidentitas` (`id_identitas`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
