/*  data picker  */

$(function() {
    for(i=1; i<9; i++) {
		$( "#datepicker"+i ).datepicker({
			numberOfMonths : 1,
			dateFormat: 'dd/mm/yy',
			changeMonth : true,
			changeYear : true,
			onClose: function(dateText) {
               dateVariable = dateText;
               
            }
		});
    }
});

/*  validasi jumlah aak  */

<!-- js anak */ -->
$(function() {
 $("#jmlanak").change(function(){

    // The easiest way is of course to delete all textboxes before adding new ones
    //$("#holder").html("");

    var count = $("#holder input").size();
    var requested = parseInt($("#jmlanak").val(),10);
    if(requested < 0 ){
    var x = requested;
        $("#holder input").remove();
        $("#holder label").remove();
        $("#holder br").remove();
    }
    else{
    if (requested > count) {
        for(i=count; i<requested; i++) {
          ci=i+1;
        var $ctrl = '<label>Nama Anak ke-'+ci+'</label><br/><input type=\"text\" name=\"anak['+ci+']\"  class=\"form-control\" placeholder=\"Nama anak '+ci+'\">';
            $("#holder").append($ctrl);
        }
    }
    else if (requested < count) {
        var x = requested - 1;
        $("#holder input:gt(" + x + ")").remove();
        $("#holder label:gt(" + x + ")").remove();
        $("#holder br").remove();
    }
    }
});
 });



$(document).ready(function(){
    var pesankosong = "Field ini harus diisi";
    var radioselek = "Field ini harus dipilih";
    var nama = "minimal 3 karakter , maksimal 100 karakter";
    var alamat = "minimal 3 karakter , maksimal 200 karakter";
    var kota = "minimal 3 karakter , maksimal 100 karakter";
    var kodepos = "maksimal 7 karakter";
    var notlp = "maksimal 15 karakter";
$('#form')

.bootstrapValidator({
    framework: 'bootstrap',
    icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },

    fields: {
        nama:{
            // Show the message in a tooltip
            validators: {
                    notEmpty: {
                        message: pesankosong
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: nama
                    }
                }
            },

        tempat_lahir:{
            // Show the message in a tooltip
            validators: {
                    notEmpty: {
                        message: pesankosong
                    },
                    stringLength: {
                        min: 2,
                        max:200,
                        message: alamat
                    }
                }
            },
        tgl_lahir_user:{
            // Show the message in a tooltip
            validators: {
                    notEmpty: {
                        message: pesankosong
                    }
                }
            },

         agama:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: radioselek
                        }
            }
        },

        status:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        }
            }
        },

        jk:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        }
            }
        },
	email:{
        	validators:{
        		notEmpty:{
        			message: pesankosong
        		},
        		stringLength: {
                            min: 2,
                            max:200,
                            message: alamat
                        }
        	}
        },
         alamatrumahi:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 2,
                            max:200,
                            message: alamat
                        }
            }
        },

        kotai:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 2,
                            max:100,
                            message: kota
                        }
            }
        },


        provinsii:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 2,
                            max:100,
                            message: kota
                        }
            }
        },


        Kode_posi:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 2,
                            max:7,
                            message: kodepos
                        }
            }
        },

        no_tlpi:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 0,
                            max:15,
                            message: notlp
                        }
            }
        },
        /*
        data paspor
        */
        
        paspor:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        }
            }
        },
        
        nomor_paspor:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        }
            }
        },
        
        tempat_keluar:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        }
            }
        },
        
        
        tanggalkp:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        }
            }
        },
        
        tanggalap:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        }
            }
        },
        
        
        
 	/*
           data indentitas di thailand
        */
	alamatrumaht:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 2,
                            max:200,
                            message: alamat
                        }
            }
        },

        Kode_post:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 2,
                            max:7,
                            message: kodepos
                        }
            }
        },


        kotat:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 2,
                            max:100,
                            message: kota
                        }
            }
        },


        tlpt:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 1,
                            max:7,
                            message: notlp
                        }
            }
        },

        pekerjaant:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 1,
                            max:200,
                            message: alamat
                        }
            }
        },
        
        tmptkerjasekolah:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 1,
                            max:200,
                            message: alamat
                        }
            }
        },
        
        tlptmptkerjasekolah:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 1,
                            max:15,
                            message: notlp
                        }
            }
        },
        
        alamatkantorsekolah:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 1,
                            max:200,
                            message: alamat
                        }
            }
        },
        
        Kodeposkantorsekolah:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 1,
                            max:7,
                            message: kodepos
                        }
            }
        },
	
	kotakantorsekolah:{
            // Show the message in a tooltip
           validators: {
                        notEmpty: {
                            message: pesankosong
                        },
                        stringLength: {
                            min: 1,
                            max:100,
                            message: kota
                        }
            }
        },

        /*
           Data di thailand dan indonesia
        */
                    rnamalengkapht:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength: {
                                    min: 2,
                                    max: 200,
                                    message: nama
                            }
                        }
                    },

                    statushubungant:{
                        validators:{
                            notEmpty:{
                                message:pesankosong
                            },
                            stringLength: {
                                    min: 2,
                                    max: 200,
                                    message: nama
                            }
                        }
                    },

                    alamathubunganht:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength: {
                                    min: 3,
                                    max: 200,
                                    message: alamat
                            }
                        }
                    },

                    Kodeposht:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                                max: 7,
                                message: kodepos
                            }
                        }
                    },

                    kotaht:{
                        validators:{
                            notEmpty:{
                                message:pesankosong
                            },
                            stringLength:{
                                min: 3,
                                max: 100,
                                message: kota
                            }
                        }
                    },

                    notlpht:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                                max: 15,
                                message: notlp
                            }
                        }
                    },

                    nosht:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                                max: 15,
                                message: notlp
                            }
                        }
                    },

                    emailht:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            }
                        }
                    },

                    /*
                        Di indonesia
                    */
                    rnamalengkaphi:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength: {
                                    min: 2,
                                    max: 100,
                                    message: nama
                            }
                        }
                    },

                    statushubungani:{
                        validators:{
                            notEmpty:{
                                message:pesankosong
                            },
                            stringLength: {
                                    min: 2,
                                    max: 200,
                                    message: nama
                            }
                        }
                    },

                    alamathubunganhi:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength: {
                                    min: 3,
                                    max: 200,
                                    message: alamat
                            }
                        }
                    },

                    Kodeposhi:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                                max: 7,
                                message: kodepos
                            }
                        }
                    },

                    kotahi:{
                        validators:{
                            notEmpty:{
                                message:pesankosong
                            },
                            stringLength:{
                                min: 3,
                                max: 100,
                                message: kota
                            }
                        }
                    },

                    notlphi:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                                max: 15,
                                message: notlp
                            }
                        }
                    },

                    noshi:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                                max: 15,
                                message: notlp
                            }
                        }
                    },

                    emailhi:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                            	min:5,
                                max: 200,
                                message: notlp
                            }
                        }
                    },
		    
                    

                    // INFORMASI KEKONSULERAN > Tempat
                    tempattpf:{
                        validators:{
                            notEmpty:{
                                message: pesankosong
                            },
                            stringLength:{
                            	min:5,
                                max: 200,
                                message: notlp
                            }
                        }
                    },


                    //Pernyataan
                    pernyataan:{
                        validators:{
                            notEmpty:{
                                message: radioselek
                            }
                        }
                    }

                }




    
  });
});